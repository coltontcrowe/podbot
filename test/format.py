"""
format.py
=========
Tests for formatting messages
"""
from .context import podbot
from podbot.format import RollMsg

def test_init():
    data = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
    content = ['red', 'orange', 'yellow', 'green', 'blue']
    msg = RollMsg(data)

    assert msg.posts == data
    assert msg.content == content
    assert msg.total_num == 7
    assert msg.num_display == 5


def test_iter_once():
    data = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
    once = ['orange', 'yellow', 'green', 'blue', 'indigo']
    msg = RollMsg(data)
    msg.iterate()

    assert msg.posts == data
    assert msg.content == once
    assert msg.total_num == 7
    assert msg.num_display == 5

def test_iter_seven():
    data = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet']
    content = ['red', 'orange', 'yellow', 'green', 'blue']
    msg = RollMsg(data)
    msg.iterate()
    msg.iterate()
    msg.iterate()
    msg.iterate()
    msg.iterate()
    msg.iterate()
    msg.iterate()

    assert msg.posts == data
    assert msg.content == content
    assert msg.total_num == 7
    assert msg.num_display == 5
