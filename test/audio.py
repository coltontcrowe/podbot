# audio.py

import discord
import discord.ext.test as dpytest
import pytest
from decimal import Decimal


@pytest.mark.asyncio
async def test_volume(create_bot):
    bot = create_bot
    vol_cmd = 57
    vol_prediction = Decimal(vol_cmd/100)
    dpytest.configure(bot)

    await dpytest.message(f'!volume {vol_cmd}')
    
    assert Decimal(bot.voice_clients[0].source.volume) == vol_prediction

@pytest.mark.asyncio
async def test_join(create_bot):
    bot = create_bot
    dpytest.configure(bot)

    await dpytest.message(f'!join General')
    pass

@pytest.mark.asyncio
async def test_play(create_bot):
    pass

@pytest.mark.asyncio
async def test_stop(create_bot):
    pass

@pytest.mark.asyncio
async def test_pause(create_bot):
    pass

@pytest.mark.asyncio
async def test_leave(create_bot):
    pass
