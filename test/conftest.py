# conftest.py

import pytest
from discord.ext import commands
from .context import podbot

@pytest.fixture
def create_bot():
    bot = commands.Bot("!")
    audio_cog = podbot.Audio(bot)
    bot.add_cog(audio_cog)
    return bot
