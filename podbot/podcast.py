"""
podcast.py
===========
A cog used to download and display podcast episodes
"""
from mygpoclient import public
from discord.ext import commands
from urllib.parse import unquote
from .parser import Episode, Show
from .format import RollMsg
import aiohttp
import asyncio
import json
import xmltodict

URL='https://itunes.apple.com/search?term={}&entity=podcast'

class Podcast(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()
        self.pod_client = public.PublicClient()

    def cog_unload(self):
        self.session.close()

    @commands.command()
    async def find(self, ctx, target: str):
        async with self.session.get(URL.format(target)) as response:
            data = await response.read()
        podcasts = json.loads(data)['results']
        await self.print_podcast_info(ctx, podcasts)

    @commands.command()
    async def episodes(self, ctx, url: str):
        async with self.session.get(url) as response:
            data = await response.read()

        all_data = xmltodict.parse(data)['rss']['channel']['item']
        episodes = [str(Episode(ep_data)) for ep_data in all_data]
        msg = RollMsg(episodes)

        discord_post = await ctx.channel.send(str(msg))
        await asyncio.sleep(5)

        while msg.complete is False:
            await asyncio.sleep(msg.time_per_iter)
            msg.iterate()
            await discord_post.edit(content=str(msg))

    async def print_podcast_info(self, ctx, podcasts):
        for podcast in podcasts:
            show = Show(podcast)
            response = str(show)

            await ctx.send(response)
