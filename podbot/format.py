"""
parser.py
=========
contains classes used to format messages
"""



class RollMsg:
    def __init__(self, posts):
        self.posts = posts
        self.total_num = len(posts)
        self.index = 0
        self.eps_shown = 0
        self.loops = 2
        self.num_display = 5
        self.msg_per_iter = 1
        self.time_per_iter = 5
        self.complete = False
        self.content = None

        self.gen_content()

    def __str__(self):
        return '\n'.join(self.content)

    def iterate(self):
        self.index += self.msg_per_iter
        self.index %= self.total_num
        self.gen_content()

        self.eps_shown += self.msg_per_iter
        if self.eps_shown//self.total_num >= self.loops:
            self.complete = True

    def gen_content(self):
        start = self.index
        end = self.index + self.num_display

        if end > self.total_num:
            first_half = self.posts[start:self.total_num+1]
            second_half = self.posts[0:end+1]
            self.content = first_half + second_half
        else:
            self.content = self.posts[start:end]
