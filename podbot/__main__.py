#! /usr/bin/env pipenv-shebang
# __main__.py

import os
import aiohttp
from .audio import Audio
from .podcast import Podcast
from dotenv import load_dotenv
from discord.ext import commands


if __name__ == '__main__':
    load_dotenv()
    token = os.getenv('DISCORD_TOKEN')
    prefix = os.getenv('BOT_PREFIX')
    bot = commands.Bot(command_prefix=prefix)

    @bot.event
    async def on_ready():
        print('Connected!')
        print('Username: {0.name}\nID: {0.id}'.format(bot.user))

    bot.add_cog(Audio(bot))
    bot.add_cog(Podcast(bot))
    bot.run(token)
