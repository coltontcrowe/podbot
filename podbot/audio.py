"""
bot.py
==============
A discord bot for turning a voice channel into a radio.
"""

import os
import discord
import asyncio
import aiohttp
from dotenv import load_dotenv
from discord.ext import commands

class Audio(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.session = aiohttp.ClientSession()

    @commands.command(name='join')
    async def join(self, ctx, *, channel: discord.VoiceChannel):
        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)
        await channel.connect()

    @commands.command()
    async def play(self, ctx, *, query):
        source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(query))
        ctx.voice_client.play(source)
        await ctx.send(f'Now playing {query}')

    async def pause(self, ctx):
        if ctx.voice_client is None:
            return await ctx.send('Not connected to voice channel')
        await ctx.voice_client.source.pause()

    async def stop(self, ctx):
        if ctx.voice_client is None:
            return await ctx.send('Not connected to voice channel')
        await ctx.voice_client.source.stop()
        
    @commands.command()
    async def volume(self, ctx, volume: int):
        """User command to set the audio volume
        """
        if ctx.voice_client is None:
            return await ctx.send('Not connected to voice channel')

        ctx.voice_client.source.volume = volume / 100
        await ctx.send(f'Changed volume to {volume}%')

    @commands.command()
    async def leave(self, ctx):
        client = ctx.message.guild.voice_client
        await client.disconnect()

    @commands.command()
    async def kill(self, ctx):
        guild = ctx.message.guild
        await guild.leave()
