"""
parser.py
utility functions to help parse xml from websites
"""
import hashlib

EPISODE='''Episode: {}
Key: {}
#######################
'''


SHOW='''{}
By: {}
Link: {}
#######################
'''


class Episode:

    def __init__(self, data):
        self.title = data['title']
        self.desc = data['description']
        url = data['link']
        self.url = url
        self.key = hashlib.md5(url.encode()).hexdigest()

    def __str__(self):
        return EPISODE.format(self.title, self.key)

    def has_prefix(self, pref):
        return self.key.startswith(pref)


class Show:
    def __init__(self, data):
        self.title = data['collectionName']
        self.author = data['artistName']
        self.url = data['feedUrl']

    def __str__(self):
        return SHOW.format(self.title, self.author, self.url)

class Parser:
    """Collects podcast info from url and formats it"""
    @classmethod
    async def create(cls, feed):
        self = Parser()
        self.session = aiohttp.ClientSession()
        self.feed_url = feed
        async with self.session.get(feed) as response:
            data = await response.read()
        self.episodes = xmltodict.parse(data)['rss']['channel']['item']

        return self

    def __init__(self, feed):
        self.feed_url = feed
        
        data = None
